<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Books;
use Illuminate\Http\Request;
// use Illuminate\Support\Facades\DB;
// use Illuminate\Database\QueryException as Exception;

class BooksController extends Controller
{

    public function index()
    {
        $books = Books::all();
        return response()->json($books);
    }

    public function show($id)
    {
        $book = Books::find($id);
        return response()->json($book);
    }

    public function store(Request $request)
    {
        $books = Books::create($request->all());
        return response()->json($books);
    }

    public function destroy($id)
    {
        $book = Books::find($id);
        $book->delete();
        return response()->json('succes');
    }


    public function update(Request $request, $id)
    {
        $book = Books::find($id);
        $book->title = $request->input('title');
        $book->writer = $request->input('writer');
        $book->save();
        return response()->json($book);

    }


}

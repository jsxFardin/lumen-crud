<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Article;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException as Exception;
// use Illuminate\Support\Facades\Request;

class ArticleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

	// store method with validation
    public function store(Request $request)
    {
		try {
			$rules = array(
				"title" => 'required',
				"content" => 'required',
			);
			$validator = Validator::make($request->all(), $rules);
			if ($validator->fails()) {
				$json = [
					'success' => false,
					'errors' => $validator->messages()
				];
				return response()->json($json, 400);
			}
			$article = Article::create($request->all());

			return response()->json($article);
			
		} catch (Exception $e) {
            return response()->json($e->getMessage(), 403);
        }
    }
    public function index()
    {

        $articles = Article::all();
        return $articles;

    }
    public function show($id)
    {
        try {
            $articles = Article::find($id);
            return response()->json($articles);

        } catch (Exception $e) {
            return response()->json($e->getMessage(), 403);
        }
    }

    public function destroy($id)
    {
        try {

            $article = Article::find($id);
            $article->delete();
            return response()->json($article);

        } catch (Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }

    public function update(Request $request, $id)
    {
        
        try {

            $article = Article::find($id);

            $article->title = $request->input('title');
            $article->content = $request->input('content');
            $article->save();

            return response()->json($article);

        } catch (Exception $e) {

            return response()->json($e->getMessage(), 500);
        }
    }
}

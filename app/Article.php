<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

// artiles
class Article extends Model{

    protected $fillable = ['title', 'content'];

}